#!/usr/bin/env bash

## https://docs.gitlab.com/charts/installation/cloud/gke.html
## https://docs.gitlab.com/charts/installation/deployment.html

environment () {
  HELMPATH=$(which helm)
  if [ "${HELMPATH}" == "" ]; then
    echo "You must have helm installed and have done a 'helm init' to run this script."
    exit 1
  fi

  # Set values that will be overwritten if env.sh exists
  echo "Setting up the environment..."
  export DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  export REGION='us-central1'
  export ZONE='us-central1-f'
  export CLUSTER_NAME='gitlab-cluster'
  export EMAIL='$(gcloud config list account --format "value(core.account)")'
  export PROJECT_ID=$(gcloud config get-value project)
  export PROJECT_NUMBER=$(gcloud projects list --filter="${PROJECT_ID}" --format="value(PROJECT_NUMBER)")

  [[ -f "${DIR}/env.sh" ]] && echo "Importing environment from ${DIR}/env.sh..." && . ${DIR}/env.sh
  echo "Writing ${DIR}/env.sh..."
  cat >> ${DIR}/env.sh << EOF
export REGION=${REGION}
export ZONE=${ZONE}
export CLUSTER_NAME=${CLUSTER_NAME}
export PROJECT_ID=${PROJECT_ID}
export PROJECT_NUMBER=${PROJECT_NUMBER}
EOF
}

# execute GitLab commands commands
# Using https://gitlab.com/gitlab-org/charts/gitlab/-/tree/master/scripts

#./gitlab/gke_bootstrap_script.sh up


### SETTING UP GKE



gke_setup () {

  set +x; echo "Enabling APIs..."
  set -x
  gcloud services enable iam.googleapis.com
  gcloud services enable compute.googleapis.com
  gcloud services enable containerregistry.googleapis.com
  gcloud services enable artifactregistry.googleapis.com
  gcloud services enable container.googleapis.com
  gcloud services enable run.googleapis.com
  gcloud services enable cloudbuild.googleapis.com
  gcloud services enable datastore.googleapis.com
  gcloud services enable firestore.googleapis.com

  set +x; echo; set -x

  set +x; echo "Creating gitlab cluster..."
  set -x
  gcloud container clusters create gitlab-cluster \
      --zone ${ZONE} \
      --addons=HttpLoadBalancing \
      --release-channel=regular --cluster-version 1.19 \
      --machine-type n1-standard-4 \
      --scopes cloud-platform \
      --num-nodes 3\
      --enable-ip-alias \
      --project ${PROJECT_ID}

  echo "Waiting for cluster bring up..."
  sleep 15


  # Connect to cluster
  set +x; echo "Connect to cluster.."
  set -x;
  gcloud container clusters get-credentials gitlab-cluster --zone ${ZONE} --project ${PROJECT_ID}
  set +x; echo


  set +x; echo "Set up bindings.."
  set -x
  kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user=$(gcloud config get-value core/account)
  


  sleep 60

}



### SETTING UP GITLAB

gitlab_setup () {

  # https://github.com/GoogleCloudPlatform/click-to-deploy/tree/master/k8s/gitlab
  set +x; echo "Installing gitlab into cluster using Marketplace.."
  set -x
  kubectl apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"
  git clone --recursive https://github.com/GoogleCloudPlatform/click-to-deploy.git

  cd ../click-to-deploy/k8s/gitlab


  #export GL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  export GL_DIR=${HOME}'/tekton-google-workshop/click-to-deploy/k8s/gitlab'
  # Setting up Variables

  export GITLAB_INSTANCE_NAME=gitlab-1
  export GITLAB_NS=default

  export GITLAB_TAG="13.5"


  export IMAGE_REGISTRY="marketplace.gcr.io/google"

  export IMAGE_GITLAB="${IMAGE_REGISTRY}/gitlab"
  export IMAGE_REDIS="${IMAGE_REGISTRY}/gitlab/redis:${GITLAB_TAG}"
  export IMAGE_REDIS_EXPORTER="${IMAGE_REGISTRY}/gitlab/redis-exporter:${GITLAB_TAG}"
  export IMAGE_POSTGRESQL="${IMAGE_REGISTRY}/gitlab/postgresql:${GITLAB_TAG}"
  export IMAGE_POSTGRESQL_EXPORTER="${IMAGE_REGISTRY}/gitlab/postgresql-exporter:${GITLAB_TAG}"
  export IMAGE_DEPLOYER="${IMAGE_REGISTRY}/gitlab/deployer:${GITLAB_TAG}"
  export IMAGE_METRICS_EXPORTER="${IMAGE_REGISTRY}/gitlab/prometheus-to-sd:${GITLAB_TAG}"

  # Set alias for password generation
  alias generate_pwd="cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 20 | head -n 1 | tr -d '\n'"

  # Generate password for GitLab, Redis, and PostgreSQL
  export GITLAB_ROOT_PASSWORD="$(generate_pwd)"
  export REDIS_ROOT_PASSWORD="$(generate_pwd)"
  export POSTGRES_PASSWORD="$(generate_pwd)"
  export DEFAULT_STORAGE_CLASS="standard-rwo" 
  export METRICS_EXPORTER_ENABLED=false

  unset DOMAIN_NAME
  export SSL_CONFIGURATION="Default"

  openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
      -keyout /tmp/tls.key \
      -out /tmp/tls.crt \
      -subj "/CN=gitlab/O=gitlab"


  export TLS_CERTIFICATE_KEY="$(cat /tmp/tls.key | base64)"
  export TLS_CERTIFICATE_CRT="$(cat /tmp/tls.crt | base64)"


  # Create Kubernetes Secrets!
  kubectl create secret tls ${GITLAB_INSTANCE_NAME}-tls \
    --cert=/tmp/tls.crt \
    --key=/tmp/tls.key


  export GITLAB_SERVICE_ACCOUNT="${GITLAB_INSTANCE_NAME}-serviceaccount"
  kubectl create serviceaccount "${GITLAB_SERVICE_ACCOUNT}" --namespace "${GITLAB_NS}"
  kubectl create clusterrole "${GITLAB_SERVICE_ACCOUNT}-role" --verb=get,list,watch --resource=services,nodes,pods,namespaces
  kubectl create clusterrolebinding "${GITLAB_SERVICE_ACCOUNT}-rule" --clusterrole="${GITLAB_SERVICE_ACCOUNT}-role" --serviceaccount="${GITLAB_NS}:${GITLAB_SERVICE_ACCOUNT}"


  # Helm setup.
  #helm repo add gitlab https://charts.gitlab.io/
  #helm repo update
  helm template ${GITLAB_INSTANCE_NAME} ${GL_DIR}/chart/gitlab \
    --namespace "${GITLAB_NS}" \
    --set gitlab.image.repo="${IMAGE_GITLAB}" \
    --set gitlab.image.tag="${GITLAB_TAG}" \
    --set gitlab.rootPassword="${GITLAB_ROOT_PASSWORD}" \
    --set gitlab.serviceAccountName="${GITLAB_SERVICE_ACCOUNT}" \
    --set gitlab.domainName="${DOMAIN_NAME}" \
    --set gitlab.sslConfiguration="${SSL_CONFIGURATION}" \
    --set redis.image="${IMAGE_REDIS}" \
    --set redis.exporter.image="${IMAGE_REDIS_EXPORTER}" \
    --set redis.password="${REDIS_ROOT_PASSWORD}" \
    --set postgresql.image="${IMAGE_POSTGRESQL}" \
    --set postgresql.exporter.image="${IMAGE_POSTGRESQL_EXPORTER}" \
    --set postgresql.password="${POSTGRES_PASSWORD}" \
    --set persistence.storageClass="${DEFAULT_STORAGE_CLASS}" \
    --set deployer.image="${IMAGE_DEPLOYER}" \
    --set metrics.image="${IMAGE_METRICS_EXPORTER}" \
    --set tls.base64EncodedPrivateKey="${TLS_CERTIFICATE_KEY}" \
    --set tls.base64EncodedCertificate="${TLS_CERTIFICATE_CRT}" \
    > "${DIR}/${GITLAB_INSTANCE_NAME}_manifest.yaml"

  kubectl apply -f "${DIR}/${GITLAB_INSTANCE_NAME}_manifest.yaml" --namespace "${GITLAB_NS}"

  sleep 60 

  export GITLAB_IP=$(kubectl get svc ${GITLAB_INSTANCE_NAME}-gitlab-svc --output jsonpath='{.status.loadBalancer.ingress[0].ip}')


  gcloud compute addresses create ${CLUSTER_NAME}-external-ip --addresses ${GITLAB_IP} --region ${REGION} --project ${PROJECT_ID}

  export GITLAB_DOMAIN=${GITLAB_IP}'.nip.io'

    set +x; echo

    sleep 90

  ### Apply GCE Ingress

    set +x; echo "Setting up GitLab NGINX Ingress ..."
    set -x


#  cat <<EOF | kubectl apply -f -
#  apiVersion: networking.k8s.io/v1beta1
# kind: Ingress
#  metadata:
#    name: gitlab-ingress
#    annotations:
#      # use the shared ingress-nginx
#      kubernetes.io/ingress.class: "gce"
#  spec:
#    rules:
#    - host: gitlab.${DOMAIN_NAME}
#      http:
#        paths:
#        - path: /
#          backend:
#            serviceName: ${GITLAB_INSTANCE_NAME}-gitlab-svc
#            servicePort: 80
#EOF


    set +x; echo

}


### SETTING UP GCP BINDINGS


gcp_bindings () {

#Give your compute service account IAM access to Secret Manager
  gcloud projects add-iam-policy-binding ${PROJECT_ID} --member serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com --role roles/secretmanager.admin

# Grant the Cloud Run Admin role to the Cloud Build service account
set +x; echo "Setting IAM Binding for Cloud Build and Cloud Run.."

set -x
gcloud projects add-iam-policy-binding $PROJECT_ID \
  --member "serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com" \
  --role roles/cloudbuild.builds.editor
set +x; echo

# Grant the IAM Service Account User role to the Cloud Build service account on the Cloud Run runtime service account
set -x
gcloud iam service-accounts add-iam-policy-binding \
  ${PROJECT_NUMBER}-compute@developer.gserviceaccount.com \
  --member="serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com" \
  --role="roles/iam.serviceAccountUser"
set +x; echo

# Grant the Cloud Run Admin role to the compute service account

set -x
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com" \
  --role roles/run.admin
set +x; echo

# Grant the IAM Service Account User role to the Compute service account on the Cloud Run runtime service account
set -x
gcloud iam service-accounts add-iam-policy-binding \
  ${PROJECT_NUMBER}-compute@developer.gserviceaccount.com \
  --member="serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com" \
  --role="roles/iam.serviceAccountUser"
set +x; echo

# Grant the Cloud Run Admin role to the Cloud Build service account
set -x
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${PROJECT_NUMBER}@cloudbuild.gserviceaccount.com" \
  --role roles/run.admin
set +x; echo

# Grant the IAM Service Account User role to the Cloud Build service account on the Cloud Run runtime service account
set -x
gcloud iam service-accounts add-iam-policy-binding \
  ${PROJECT_NUMBER}-compute@developer.gserviceaccount.com \
  --member="serviceAccount:${PROJECT_NUMBER}@cloudbuild.gserviceaccount.com" \
  --role="roles/iam.serviceAccountUser"
set +x; echo
}


#Main
environment
gke_setup
gitlab_setup
gcp_bindings

sleep 30

set +x; clear
set +x; echo


echo 'your username is: root'
echo 'your password is: ' ${GITLAB_ROOT_PASSWORD}
echo 'Please visit http://'${GITLAB_DOMAIN}' in your browser'
